<?php


namespace Pff\FirewallClientTests\Unit;

use Pff\FirewallClient\Driver\AdvFirewallDriver;
use Pff\FirewallClient\FirewallClientFactory;
use Pff\FirewallClientTests\TestCase;

class FirewallTest extends TestCase
{
    /**
     * @var FirewallClientFactory
     */
    protected $factory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->factory = new FirewallClientFactory;
    }

    public function testWindowsFirewall()
    {
        $firewall = $this->factory->make(['driver' => 'advfirewall'], 'advfirewall');
        $this->assertInstanceOf(AdvFirewallDriver::class, $firewall);
    }

    public function testExecuteString()
    {
        $config = [
            'driver' => 'advfirewall',
            'name' => 'name',
            'prefix' => 'prefix_',
            'service' => 'serviceName_{PORT}',
            'allow' => false,
            'ips' => ['192.168.1.100', '192.168.0.1/24', '192.168.1.0-192.168.10.255'],
            'ports' => '3389',
            'protocol' => 'TCP'
        ];
        /* @var AdvFirewallDriver */
        $firewall = $this->factory->make($config);

//        var_dump($firewall);
        $this->assertSame(
            ['netsh advfirewall firewall add rule name="prefix_serviceName_3389" dir=in action="block" remoteip="192.168.1.100,192.168.0.0/24,192.168.1.0-192.168.10.255" localport="3389" protocol="TCP"'],
            $firewall->buildAdd()
        );

        $this->assertSame(
            ['netsh advfirewall firewall delete rule name="prefix_serviceName_3389" dir=in localport="3389" protocol="TCP"'],
            $firewall->buildDelete()
        );

        $this->assertSame(
            ['netsh advfirewall firewall show rule name="prefix_serviceName_3389" dir=in'],
            $firewall->buildGet()
        );

        $this->assertSame(
            ['netsh advfirewall firewall set rule name="prefix_serviceName_3389" dir=in new remoteip="192.168.1.100,192.168.0.0/24,192.168.1.0-192.168.10.255" localport="3389" protocol="TCP"'],
            $firewall->buildSet()
        );
    }
}
