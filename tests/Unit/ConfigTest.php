<?php


namespace Pff\FirewallClientTests\Unit;

use Pff\FirewallClient\Config;
use Pff\FirewallClientTests\TestCase;

class ConfigTest extends TestCase
{
    public function testConfig()
    {
        $options = [
            'driver' => 'advfirewall',
            'name' => 'name',
            'prefix' => 'prefix_',
            'allow' => false,
            'ips' => ['192.168.1.100', '192.168.0.1/24', '192.168.1.0-192.168.10.255'],
            'ports' => '3389',
            'protocol' => 'ICMP'
        ];
        $config = new Config($options);
        $this->assertInstanceOf(Config::class, $config);

        $this->assertSame("advfirewall", $config->getDriver());
        $this->assertSame("prefix_", $config->getPrefix());
        $this->assertSame("name", $config->getName());
        $this->assertSame(false, $config->getAllow());
        $this->assertSame("3389", $config->getPorts());
        $this->assertSame("ICMP", $config->getProtocol());
        $this->assertSame(['192.168.1.100', '192.168.0.0/24', '192.168.1.0-192.168.10.255'], $config->getIps());

        $config->setDriver('iptables');
        $this->assertSame("iptables", $config->getDriver());

        $config->setPrefix('pff_');
        $this->assertSame("pff_", $config->getPrefix());

        $config->setName('test_name');
        $this->assertSame("test_name", $config->getName());

        $config->setAllow(true);
        $this->assertSame(true, $config->getAllow());

        $config->setPorts("50:60");
        $this->assertSame("50:60", $config->getPorts());

        $config->setProtocol('udp');
        $this->assertSame("UDP", $config->getProtocol());

        $config->setIps(['192.168.1.100', '192.168.0.0/24']);
        $this->assertSame(['192.168.1.100', '192.168.0.0/24'], $config->getIps());
        $config->addIp('192.168.1.0-192.168.10.255');
        $this->assertSame(['192.168.1.100', '192.168.0.0/24', '192.168.1.0-192.168.10.255'], $config->getIps());
    }
}
