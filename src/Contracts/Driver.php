<?php


namespace Pff\FirewallClient\Contracts;


use Illuminate\Support\Str;
use Pff\FirewallClient\Config;
use Symfony\Component\Process\Process;


abstract class Driver
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Process
     */
    protected $process;

    /**
     * @var Process[]
     */
    protected $processes;

    public function __construct(array $config = [])
    {
        $this->config = new Config($config);
    }

    /**
     * @param null $name
     * @param null $value
     * @return Config|string|array
     */
    public function config($name = null, $value = null)
    {
        if (is_null($name)) {
            return $this->config;
        }

        if (is_null($value)) {
            return $this->config[$name] ?? null;
        }

        $setter = 'set' . Str::ucFirst(Str::camel($name));
        return $this->config->{$setter}($value);
    }

    /**
     * @return Process
     */
    public function process(): Process
    {
        return $this->process;
    }

    /**
     * @return Process[]
     */
    public function processes(): array
    {
        return $this->processes;
    }

    /**
     * Build add command line string
     *
     * @return string[]
     */
    abstract public function buildAdd(): array;

    /**
     * Build delete command line string
     *
     * @return string[]
     */
    abstract public function buildDelete(): array;

    /**
     * Build get the command line string
     *
     * @return string[]
     */
    abstract public function buildGet(): array;

    /**
     * Build update command line string
     *
     * @return string[]
     */
    abstract public function buildSet(): array;

    /**
     * Get executable path
     *
     * @return string
     */
    public function path(): string
    {
        if (is_executable($path = getcwd())) {
            return $path;
        }

        return sys_get_temp_dir();
    }

    /**
     * @param $commands
     * @return int|null
     */
    protected function run($commands): ?int
    {
        $this->processes = [];
        foreach ($commands as $command) {
            $process = Process::fromShellCommandline($command, $this->path(), null, null, null);
            $this->process = $process;
            $this->processes[] = $process;
                $process->run();
            if (! $process->isSuccessful()) {
                return $process->getExitCode();
            }
        }

        return $this->process()->getExitCode();
    }

    /**
     *
     * @return int
     */
    public function runAdd(): ?int
    {
        return $this->run($this->buildAdd());
    }

    /**
     * @return int
     */
    public function runDelete(): ?int
    {
        return $this->run($this->buildDelete());
    }

    /**
     * @return int
     */
    public function runGet(): ?int
    {
        return $this->run($this->buildGet());
    }

    /**
     * @return int
     */
    public function runSet(): ?int
    {
        return $this->run($this->buildSet());
    }
}
