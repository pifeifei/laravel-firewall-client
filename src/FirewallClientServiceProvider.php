<?php


namespace Pff\FirewallClient;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class FirewallClientServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * @inheritDoc
     */
    public function __destruct()
    {
        file_put_contents(__FILE__ . '.log', date('Y-m-d H:i:s') . ' ' . __METHOD__ . PHP_EOL, FILE_APPEND);
    }

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/firewall-client.php' => config_path('firewall-client.php'),
            ], 'firewall-client-config');
            $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        }
        file_put_contents(__FILE__ . '.log', date('Y-m-d H:i:s') . ' ' . __METHOD__ . PHP_EOL, FILE_APPEND);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // 不发布为什么读取不到配置？
        $this->mergeConfigFrom(__DIR__.'/../config/firewall-client.php', 'firewall-client');

        $this->registerClientServices();
        $this->registerAliases();

        file_put_contents(__FILE__ . '.log', date('Y-m-d H:i:s') . ' ' . __METHOD__ . PHP_EOL, FILE_APPEND);
    }

    /**
     * Register the primary firewall bindings.
     *
     * @return void
     */
    protected function registerClientServices()
    {
        $this->app->singleton('fc.factory', function () {
            return new FirewallClientFactory();
        });

        $this->app->singleton('fc', function ($app) {
            return new FirewallClientManager($app, $app['fc.factory']);
        });
    }

    /**
     * Register the core class aliases in the container.
     *
     * @return void
     */
    public function registerAliases()
    {
        file_put_contents(__FILE__ . '.log', date('Y-m-d H:i:s') . ' ' . __METHOD__ . PHP_EOL, FILE_APPEND);
        foreach ([
             'fc' => [FirewallClientManager::class],
        ] as $key => $aliases) {
            foreach ($aliases as $alias) {
                $this->app->alias($key, $alias);
            }
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['fc', 'fc.factory'];
    }
}
