<?php


namespace Pff\FirewallClient;


use Illuminate\Support\Arr;
use InvalidArgumentException;
use Pff\FirewallClient\Driver\AdvFirewallDriver;
use Pff\FirewallClient\Driver\FirewallCmdDriver;
use Pff\FirewallClient\Driver\IptablesDriver;

/**
 * Class FirewallClientFactory
 *
 * @see \Pff\FirewallClient\Contracts\Driver
 */
class FirewallClientFactory
{

    public function make(array $config, $name = null)
    {
        if (! isset($config['driver'])) {
            throw new InvalidArgumentException('A driver must be specified.');
        }

        $name = $name ?: Arr::get($config, 'driver');

        $config = $this->parseConfig($config, $name);

        return $this->createClient($config);
    }

    /** 60
     * Parse and prepare the firewall configuration.
     *
     * @param array $config
     * @param string $name
     * @return array
     */
    protected function parseConfig(array $config, string $name)
    {
        return Arr::add($config, 'name', $name);
    }

    protected function createClient(array $config)
    {

        switch ($config['driver']) {
            case 'advfirewall':
                return new AdvFirewallDriver($config);
            case 'iptables':
                return new IptablesDriver($config);
            case 'firewall':
                return new FirewallCmdDriver($config);
        }

        throw new InvalidArgumentException("Unsupported driver [{$config['driver']}].");
    }
}
