<?php

namespace Pff\FirewallClient\Driver;

use Illuminate\Support\Str;
use Pff\FirewallClient\Contracts\Driver;

class IptablesDriver extends Driver
{

    /**
     * @inheritDoc
     */
    public function buildAdd(): array
    {
        $commands = [];

//        iptables -I INPUT  1 -p tcp -d 192.168.1.108/24 --dport 80 -j ACCEPT
        foreach ($this->config->getIps() as $ip) {
            $commands[] = sprintf(
                'iptables -I INPUT  1 -p %s -d %s --dport %s -j %s',
                $this->formatProtocol(),
                $this->formatIp($ip),
                $this->formatPorts(),
                $this->formatAllow()
            );
        }

        array_push($commands, $this->buildCommandSave(), $this->buildCommandRestart());
        return $commands;
    }

    /**
     * @inheritDoc
     */
    public function buildDelete(): array
    {
        $commands = [];

//       iptables -D  INPUT -d 192.168.1.108/32 -p tcp -m tcp --dport 80 -j ACCEPT
        foreach ($this->config->getIps() as $ip) {
            $commands[] = sprintf(
                'iptables -D INPUT -d %s -p %s -m %s --dport %s -j %s',
                $this->formatIp($ip),
                $this->formatProtocol(),
                $this->formatProtocol(),
                $this->formatPorts(),
                $this->formatAllow()
            );
        }

        array_push($commands, $this->buildCommandSave(), $this->buildCommandRestart());
        return $commands;
    }

    /**
     * @inheritDoc
     */
    public function buildGet(): array
    {
        return [
            sprintf(
                'iptables -S | grep \'%s\' | grep \'%s\' | grep \'%s\'',
                '\\-\\-dport ' . $this->formatPorts(),
                '\\-p ' . $this->formatProtocol(),
                '\\-j ' . $this->formatAllow()
            )
        ];
    }

    /**
     * @inheritDoc
     */
    public function buildSet(): array
    {
//        $commands = [
//            // TODO: 查询已有的规则并删除
////            sprintf(
////                'iptables -S | grep \'%s\' | grep \'%s\' | grep \'%s\'',
////                '\\-\\-dport ' . $this->formatPorts(),
////                '\\-p ' . $this->formatProtocol(),
////                '\\-j ' . $this->formatAllow()
////            )
//        ];
//        array_push($commands, ...($this->buildAdd()));
        return $this->buildAdd();
    }


    /**
     * @param string $ip
     * @return string
     */
    protected function formatIp(string $ip): string
    {
        //TODO: 192.168.1.0-192.168.10.255 => 192.168.1.0/24,192.168.2.0/24 ... 192.168.10.0/24,
        return $ip;
    }

    protected function buildCommandSave()
    {
        return 'service iptables save';
    }

    protected function buildCommandRestart()
    {
        return 'service iptables restart';
    }

    protected function formatAllow(): string
    {
        return $this->config()->getAllow() ? 'ACCEPT' : 'DROP'; //REJECT
    }

    protected function formatPorts()
    {
        $ports = $this->config()->getPorts();
        return Str::contains($ports, '-') ? str_replace('-', ':', $ports) : $ports;
    }

    protected function formatProtocol(): string
    {
        return strtolower($this->config()->getProtocol());
    }
}
