<?php

namespace Pff\FirewallClient\Driver;

use Pff\FirewallClient\Contracts\Driver;

class AdvFirewallDriver extends Driver
{
    /**
     * @inheritDoc
     */
    public function buildAdd(): array
    {
        //netsh advfirewall firewall add rule name=test_port action=allow dir=in protocol=TCP localport=19999 remoteip=192.168.1.0/24,8.8.8.8,192.168.10.0-192.168.20.255
        //netsh advfirewall firewall add rule name=prefix_name dir=in action=bypass remoteip=192.168.1.100 localport=3389 protocol=TCP
        return [sprintf(
            'netsh advfirewall firewall add rule name="%s" dir=in action="%s" remoteip="%s" localport="%s" protocol="%s"',
            $this->config->getService(),
            $this->formatAllow($this->config->getAllow()),
            $this->formatIps($this->config->getIps()),
            $this->config->getPorts(),
            $this->config->getProtocol()
        )];
    }

    /**
     * @inheritDoc
     */
    public function buildDelete(): array
    {
        //netsh advfirewall firewall delete rule name=test_port  dir=in protocol=TCP localport=19999 remoteip=192.168.1.0/24,8.8.8.8,192.168.10.0-192.168.20.255
        return [sprintf(
            'netsh advfirewall firewall delete rule name="%s" dir=in localport="%s" protocol="%s"',
            $this->config->getService(),
            $this->config->getPorts(),
            $this->config->getProtocol()
        )];
    }

    /**
     * @inheritDoc
     */
    public function buildGet(): array
    {
        //netsh advfirewall firewall show rule name=test_port dir=in localport=19999 protocol=TCP
        return [sprintf(
            'netsh advfirewall firewall show rule name="%s" dir=in',
            $this->config->getService()
        )];
    }

    /**
     * @inheritDoc
     */
    public function buildSet(): array
    {
        //netsh advfirewall firewall set rule name=test_port  dir=in protocol=TCP localport=19999 remoteip=192.168.1.0/24,8.8.8.8,192.168.10.0-192.168.20.255  new remoteip=192.168.30.0-192.168.50.255
        //netsh advfirewall firewall set rule name=test_port  dir=in new remoteip=192.168.30.0-192.168.50.255
        return [sprintf(
            'netsh advfirewall firewall set rule name="%s" dir=in new remoteip="%s" localport="%s" protocol="%s"',
            $this->config->getService(),
            $this->formatIps($this->config->getIps()),
            $this->config->getPorts(),
            $this->config->getProtocol()
        )];
    }

    protected function formatAllow(bool $allow)
    {
        return $allow ? 'allow' : 'block';
    }

    protected function formatIps(array $ips)
    {
        return implode(',', $ips);
    }
}
