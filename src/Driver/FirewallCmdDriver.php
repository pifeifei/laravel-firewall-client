<?php

namespace Pff\FirewallClient\Driver;

use Illuminate\Support\Str;
use Pff\FirewallClient\Contracts\Driver;

// 可参考：https://github.com/witersen/SvnAdminV2.0/blob/1edb41c2580de3d65728b70097af91308577955a/02.%E5%90%8E%E7%AB%AF%E5%B7%A5%E7%A8%8B/api/app/controller/firewall.class.php
//       https://gitee.com/witersen/SvnAdminV2.0/blob/main/02.%E5%90%8E%E7%AB%AF%E5%B7%A5%E7%A8%8B/api/app/controller/firewall.class.php
class FirewallCmdDriver extends Driver
{
    /**
     * @inheritDoc
     */
    public function buildAdd(): array
    {
        // firewall-cmd --permanent --add-rich-rule='rule family="ipv4" source address="192.168.0.0/24" service name="frn_port_3333" port port="3333" protocol="tcp" accept'
        // firewall-cmd --permanent --add-rich-rule 'rule family="ipv4" source address="192.168.0.0/24" port port="3333" protocol="tcp" accept'
        // firewall-cmd --permanent --add-rich-rule 'rule family="ipv4" source address="192.168.1.108" port port="81" protocol="tcp" accept'
        // firewall-cmd --permanent --remove-rich-rule 'rule family="ipv4" source address="192.168.1.108" port port="81" protocol="tcp" accept'
        // firewall-cmd  --query-rich-rule 'rule family="ipv4" source address="192.168.0.0/24" port port="3333" protocol="tcp" accept'
        // firewall-cmd
        // firewall-cmd  --query-rich-rule 'rule family="ipv4" port port="3333" protocol="tcp" accept'

        // firewall-cmd --permanent --new-ipset="ipset-port-81:89" --type=hash:ip
        // firewall-cmd --permanent  --ipset=ipset-port-81:89 --add-entry=192.167.1.1
        // firewall-cmd --permanent --add-rich-rule 'rule family="ipv4" source ipset="ipset-port-81:89" port port="81" protocol="tcp" accept'

        $commands = [];

        if (false === $this->ipsetFileExists()) {
            $commands[] = sprintf('firewall-cmd --permanent --new-ipset=%s --type=hash:ip', $this->formatIpset());
        }
        $commands[] = sprintf('firewall-cmd --permanent --ipset=%s --set-description=\'laravel firewall client\'', $this->formatIpset());

        // firewall-cmd --permanent  --ipset=testipset --add-entry=192.167.1.1
        foreach ($this->config->getIps() as $ip) {
            $commands[] = sprintf(
                'firewall-cmd --permanent --ipset=%s --add-entry=%s',
                $this->formatIpset(),
                $this->formatIp($ip)
            );
        }

        $commands[] = sprintf(
            'firewall-cmd --permanent --add-rich-rule \'rule family="ipv4" source ipset="%s" port port="%s" protocol="%s" %s\'',
            $this->formatIpset(),
            $this->formatPorts(),
            $this->formatProtocol(),
            $this->formatAllow()
        );
        $commands[] = $this->buildReload();
        return $commands;
    }

    /**
     * @inheritDoc
     */
    public function buildDelete(): array
    {
        // firewall-cmd --permanent --remove-rich-rule 'rule family="ipv4" source address="192.168.0.0/24" port port="3333" protocol="tcp" accept'
        $commands = [];
        if ($this->ipsetFileExists()) {
            sprintf('firewall-cmd --permanent --delete-ipset=%s', $this->formatIpset());
        }
        $commands[] = sprintf(
                'firewall-cmd --permanent --remove-rich-rule \'rule family="ipv4" source ipset="%s" port port="%s" protocol="%s" %s\' ',
                $this->formatIpset(),
                $this->formatPorts(),
                $this->formatProtocol(),
                $this->formatAllow()
            );
        array_push($commands, $this->buildReload());
        return $commands;
    }

    /**
     * @inheritDoc
     */
    public function buildGet(): array
    {
        // firewall-cmd --query-rich-rule 'rule family="ipv4" source address="192.168.0.0/24" port port="3333" protocol="tcp" accept'
        // firewall-cmd --query-rich-rule 'rule family="ipv4" source address="192.168.0.0/24" port port="3333" protocol="tcp" accept'
        // firewall-cmd --permanent --list-rich-rules  | grep '"ipset-port-81:89"'
        // firewall-cmd --permanent --info-ipset=ipset-port-81:89
        $commands = [];
        if ($this->ipsetFileExists()) {
            $commands[] = sprintf('firewall-cmd --permanent --info-ipset=%s', $this->formatIpset());
        }

        $commands[] = sprintf(
            'firewall-cmd --permanent --list-rich-rules | grep \'"%s"\'',
            $this->formatIpset()
        );
        return $commands;
    }

    /**
     * @inheritDoc
     */
    public function buildSet(): array
    {
        $commands = $this->buildDelete();
        array_pop($commands);
        array_push($commands, ...($this->buildAdd()));

        return $commands;
    }

    protected function buildReload()
    {
        return 'firewall-cmd --reload';
    }

    protected function formatAllow()
    {
        return $this->config->getAllow() ? 'accept' : 'drop'; //REJECT
    }

    /**
     * @param string $ip
     * @return string
     */
    protected function formatIp(string $ip)
    {
        return $ip;
    }

    protected function formatIpset()
    {
        return sprintf('%sipset_port_%s', $this->config()->getPrefix(), $this->formatPorts());
    }

    protected function formatPorts()
    {
        $ports = $this->config->getPorts();
        return Str::contains($ports, ':') ? str_replace(':', '-', $ports) : $ports;
    }

    protected function formatProtocol()
    {
        return strtolower($this->config()->getProtocol());
    }

    /**
     * @return string
     */
    protected function ipsetFilename(): string
    {
        return sprintf('/etc/firewalld/ipsets/%s.xml', $this->formatIpset());
    }

    /**
     * @return bool
     */
    protected function ipsetFileExists(): bool
    {
        return file_exists($this->ipsetFilename());
    }
}
