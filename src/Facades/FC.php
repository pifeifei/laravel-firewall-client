<?php

namespace Pff\FirewallClient\Facades;

use Illuminate\Support\Facades\Facade;
use Pff\FirewallClient\FirewallClientManager;

/**
 * @method client()
 *
 * @see FirewallClientManager
 */
class FC extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'fc';
    }
}
