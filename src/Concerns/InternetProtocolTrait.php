<?php


namespace Pff\FirewallClient\Concerns;


trait InternetProtocolTrait
{
    //网络地址 Network address
    //主机地址 Host address
    //组播地址 Multicast Address
    //子网掩码 subnet mask
    //广播地址 Broadcast Address
    //mac Physical Address
    // Gateway

    //网络号 The network number
    //子网号

    /**
     * @param int $subnetMask
     * @return int
     */
    protected function subnetMask(int $subnetMask): int
    {
        return (0xffffffff << (32 - $subnetMask)) & 0xffffffff;
    }

    /**
     * @param int|string $ip
     * @param int $mask
     * @return string
     */
    protected function netmask($ip, int $mask): string
    {
        $netmask = $this->subnetMask($mask);
        if (is_integer($ip)) {
            return long2ip($ip & $netmask) .  "/{$mask}";
        }

        return long2ip(ip2long($ip) & $netmask) . "/{$mask}";
    }
}
