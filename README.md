# firewall client for PHP

## feature

* [x] netsh advfirewall (Windows 10)

* [x] iptables (CentOS: 7.x, 8.x, Ubuntu)

* [x] firewall-cmd (CentOS: 7.x, 8.x, Ubuntu)

## use

```php

$options = [
    'driver' => 'firewall', // 'advfirewall',
    'name' => 'test name',
    'service' => 'service_port_{PORT}',
    'prefix' => 'prefix_',
    'allow' => false,
    'ips' => ['192.168.1.100/32', '192.168.0.1/24', '192.168.1.0-192.168.10.255', '192.169.1.1/32'],
    'ports' => '80', // 81:82
    'protocol' => 'TCP' //'UDP'
];

$factory = new \Pff\FirewallClient\FirewallClientFactory();
$firewall = $factory->make($options);
// $config = $firewall->config();
// $config->addIp('114.114.114.114/24');

// 生成执行命令行
$firewall->buildAdd();
$firewall->buildDelete();
$firewall->buildGet();
$firewall->buildSet();

// 直接执行
$firewall->runAdd();
$firewall->runDelete();
$firewall->runGet();
$firewall->runSet();

// for laravel
// $firewall = app('fc')->client('advfirewall');
// $firewall = FC::client('advfirewall');
// $firewall->buildAdd();
```
